﻿Module db

    Public Function con() As OleDb.OleDbConnection
        Select Case My.Settings.pTestDb
            Case 0
                Return livedb()
            Case 1
                Return testdb()
            Case Else
                MsgBox("Δεν υπάρχει αρχικοποίηση για βάση δεδομένων", vbExclamation, "Αρέθων")
        End Select
    End Function
    Public Function livedb() As OleDb.OleDbConnection

        Dim AccessConnectionString As New OleDb.OleDbConnection
        AccessConnectionString.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Google Drive\DATABASES\ArethonLive.mdb"

        Return AccessConnectionString

    End Function

    Public Function testdb() As OleDb.OleDbConnection

        Dim AccessConnectionString As New OleDb.OleDbConnection
        AccessConnectionString.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Google Drive\DATABASES\ArethonTest.mdb"

        Return AccessConnectionString

    End Function
End Module
