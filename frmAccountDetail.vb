﻿Imports System.Data.OleDb

Public Class frmAccountDetail

    Public pAccountId As Integer
    Public pAccountDetailId As Integer
    Public pAction As Integer
    '0 = Add
    '1 = edit
    '2 = copy
    Public pAmount As Double
    Public pSignAmount As Double
    Public pReturn As Boolean
    Private Sub frmAccountDetail_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        FillcmbAccountDetailType()
        pReturn = False

        Me.dtpDate.Enabled = True
        cmbAccountDetailType.Enabled = True

        If pSignAmount > 0 Then
            cmbAccountDetailType.SelectedIndex = 1
        Else
            cmbAccountDetailType.SelectedIndex = 0
        End If

        Select Case pAction
            Case 0
                Me.txtAmount.Text = 0
                Me.dtpDate.Value = Now.Date
                Me.txtNotes.Text = ""
            Case 1
                Me.txtAmount.Text = pAmount
            Case 2
                Me.txtAmount.Text = pAmount
                Me.dtpDate.Value = Now.Date

                cmbAccountDetailType.Enabled = False
        End Select

    End Sub

    Private Sub FillcmbAccountDetailType()
        cmbAccountDetailType.Items.Clear()
        cmbAccountDetailType.Items.Add("Χρέωση")
        cmbAccountDetailType.Items.Add("Πίστωση")
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim query As String

        pAmount = CDbl(Me.txtAmount.Text)

        Select Case Me.cmbAccountDetailType.Text
            Case "Πίστωση"
                pSignAmount = pAmount
            Case "Χρέωση"
                pSignAmount = pAmount * (-1)
        End Select

        Select Case pAction
            Case 0, 2
                query = <sql>
                    insert into account_detail_tab 
                          ( AccountId,              AccountDetailDate,                          Amount,             
                            SignAmount,             Notes,                                      UserId
                           ) 
                    values( <%= pAccountId %>,      '<%= Me.dtpDate.Value.Date.ToString %>',    <%= pAmount %>,
                            <%= pSignAmount %>,      '<%= Me.txtNotes.Text %>',                 <%= frmMain.pUserId %>
                           )                   
                </sql>.Value
            Case 1
                query = <sql>
                    update account_detail_tab 
                    set AccountDetailDate = '<%= Me.dtpDate.Value.Date.ToString %>',                 
                        Amount = <%= pAmount %>,             
                        SignAmount = <%= pSignAmount %>,             
                        Notes = '<%= Me.txtNotes.Text %>'
                    where AccountDetailId = <%= pAccountDetailId %>
                        </sql>.Value
        End Select

        Dim con As New OleDbConnection
        con = db.con
        con.Open()
        Dim com As New OleDbCommand(query, con)
        Try
            com.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("Αδύνατη η αποθήκευση " & ex.Message, MsgBoxStyle.Exclamation, "Αρέθων")
            con.Close()
        Finally
            com.Dispose()
            con.Close()
            Me.Close()
            pReturn = True
        End Try

    End Sub
End Class