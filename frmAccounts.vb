﻿Imports System.Data.OleDb
Imports Infragistics.Win
Imports Infragistics.Win.UltraWinGrid

Public Class frmAccounts

    Public pAccountId As Integer
    Public pState As Integer
    '0 = Σύνοψη
    '1 = Λεπτομέρειες λογαριασμού
    WithEvents dtpDate As New DateTimePicker

    Public pAccountTypebtnList As List(Of ToolStripButton)
    Public pSynopsisDt As DataTable
    Public pDebts As Boolean

    Private Sub frmAccounts_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ToolStrip1.Items.Add(New ToolStripControlHost(dtpDate))

        Dim query As String, dt As New DataTable, adapter As OleDbDataAdapter
        query = <sql>
                    select *
                    from AccountTypes                    
                </sql>.Value

        If pDebts Then
            query = query & <sql>
                    where TypeId = 2
                </sql>.Value
        End If

        adapter = New OleDbDataAdapter(query, db.con)

        Try
            adapter.Fill(dt)
        Catch ex As Exception
            MsgBox(ex.Message, vbExclamation, "Αρέθων")
            adapter.Dispose()
            Exit Sub
        Finally
            adapter.Dispose()
        End Try

        Dim pAccountTypeBtn As ToolStripButton
        Dim pRow As DataRow
        pAccountTypebtnList = New List(Of ToolStripButton)
        For i As Integer = 0 To dt.Rows.Count - 1
            pRow = dt.Rows(i)
            pAccountTypeBtn = New ToolStripButton
            With pAccountTypeBtn
                .Text = pRow("TypeDescr").ToString
                '  .Size = New Size(150, 60)
                .Tag = pRow("TypeId")
                .CheckOnClick = True
                .Checked = pRow("Checked")

                If pDebts Then
                    .CheckOnClick = False
                End If
            End With

            pAccountTypebtnList.Add(pAccountTypeBtn)
            AddHandler pAccountTypeBtn.Click, AddressOf Me.AccountButton_Click
        Next

        If pDebts Then

        End If

        CreateSynopsis()
    End Sub

    Private Sub CreateSynopsis()
        pState = 0
        pAccountId = 0
        ShowButtons(False)

        Dim pAccountTypesString As String = ""
        For i As Integer = 0 To pAccountTypebtnList.Count - 1
            If pAccountTypebtnList(i).Checked Then
                pAccountTypesString = pAccountTypesString & pAccountTypebtnList(i).Tag & ","
            End If
        Next

        Dim query As String, adapter As OleDbDataAdapter
        pSynopsisDt = New DataTable
        query = <sql>
                    select h.BankId, b.BankDescr, h.AccountDescr, d.AccountId, sum(d.SignAmount) as TotalAmount
                    from account_detail_tab d, account_header_tab h, bank_tab b
                    where h.AccountId = d.AccountId
                    and h.BankId = b.BankId
                    and h.inUse = -1
                    and d.UserId = <%= frmMain.pUserId %>
                    and d.AccountDetailDate <%= " <=  #" %><%= Format(dtpDate.Value.Date, "MM/dd/yyyy") & "#" %>
                </sql>.Value

        If pAccountTypesString.Length > 0 Then
            pAccountTypesString = pAccountTypesString.Substring(0, pAccountTypesString.Length - 1)

            query = query & <sql>
                    and h.AccountTypeId in (<%= pAccountTypesString %>)
                </sql>.Value
        End If

        query = query & <sql>
                    group by h.BankId, b.BankDescr, h.AccountDescr, d.AccountId
                    
                    union 
                    
                    select h.BankId, b.BankDescr, h.AccountDescr, h.AccountId, 0 as TotalAmount
                    from account_header_tab h, bank_tab b
                    where h.inUse = -1
                    and h.BankId = b.BankId
                    and h.UserId = <%= frmMain.pUserId %>
                    and not exists ( select 1 from account_detail_tab d where d.AccountId = h.AccountId )
                </sql>.Value

        If pAccountTypesString.Length > 0 Then
            query = query & <sql>
                    and h.AccountTypeId in (<%= pAccountTypesString %>)
                </sql>.Value
        End If

        adapter = New OleDbDataAdapter(query, db.con)

        Try
            adapter.Fill(pSynopsisDt)
        Catch ex As Exception
            MsgBox(ex.Message, vbExclamation, "Αρέθων")
            adapter.Dispose()
            Exit Sub
        Finally
            adapter.Dispose()
        End Try

        If pSynopsisDt.Rows.Count = 0 Then Exit Sub

        Dim pTotal As String = pSynopsisDt.Compute("sum(TotalAmount)", "").ToString
        If IsDBNull(pTotal) Then pTotal = 0
        lblDescr.Text = "Σύνολο :"
        lblTotal.Text = FormatCurrency(pTotal)

        Me.uGrd.Hide()

        Dim pText As Label
        Dim pBtn As MyButton
        Dim pBankDt As New DataTable
        pBankDt = pSynopsisDt.DefaultView.ToTable(True, "BankId", "BankDescr").DefaultView.ToTable
        Dim pBankId As Integer
        Dim pBankDescr As String

        Dim pRow As DataRow

        For i As Integer = Me.Panel1.Controls.Count - 1 To 0 Step -1
            If Me.Panel1.Controls(i).ToString = "Infragistics.Win.UltraWinGrid.UltraGrid" Then
                Continue For
            Else
                Me.Panel1.Controls.Remove(Me.Panel1.Controls(i))
            End If
        Next

        Dim pBankAccountRows As DataRow()
        For i As Integer = 0 To pBankDt.Rows.Count - 1
            pBankId = pBankDt.Rows(i)("BankId")
            pBankDescr = pBankDt.Rows(i)("BankDescr")

            pText = New Label
            With pText
                '.ReadOnly = True
                ' .Enabled = False
                '.AcceptsTab = False
                .TextAlign = HorizontalAlignment.Center
                .Size = New Size(150, 15)
                .Location = New Point(5 + 150 * i, 5)
                .Text = pBankDescr
                .BackColor = Color.Gray
            End With
            Me.Panel1.Controls.Add(pText)

            pBankAccountRows = pSynopsisDt.Select("BankId = " & pBankId)

            For j As Integer = 0 To pBankAccountRows.Length - 1
                pRow = pBankAccountRows(j)
                pBtn = New MyButton
                With pBtn
                    .Text = pRow("AccountDescr").ToString & vbNewLine & FormatCurrency(pRow("TotalAmount"))
                    .Size = New Size(150, 60)
                    .Location = New Point(5 + 150 * i, 20 + j * 60)
                    .Tag = pRow("AccountId")
                End With
                Me.Panel1.Controls.Add(pBtn)
                AddHandler pBtn.DoubleClick, AddressOf Me.Button_DoubleClick
            Next
        Next
    End Sub
    Private Sub FilluGrd(pAccountId As Integer)
        pState = 1
        ShowButtons(True)

        Dim query As String, dt As New DataTable, adapter As OleDbDataAdapter
        query = <sql>
                    select b.BankDescr, h.AccountDescr, sum(d.SignAmount) as TotalAmount
                    from account_detail_tab d, account_header_tab h, bank_tab b
                    where h.AccountId = d.AccountId
                    and h.BankId = b.BankId
                    and h.inUse = -1
                    and d.AccountId =  <%= pAccountId %>
                    and d.UserId =  <%= frmMain.pUserId %>
                    and d.AccountDetailDate <%= " <=  #" %><%= Format(dtpDate.Value.Date, "MM/dd/yyyy") & "#" %>
                    group by h.BankId, b.BankDescr, h.AccountDescr, d.AccountId                    
                </sql>.Value
        adapter = New OleDbDataAdapter(query, db.con)

        Try
            adapter.Fill(dt)
        Catch ex As Exception
            MsgBox(ex.Message, vbExclamation, "Αρέθων")
            Exit Sub
        Finally
            adapter.Dispose()
        End Try

        If dt.Rows.Count = 0 Then
            Me.uGrd.DataSource = Nothing
            Me.uGrd.Show()
            Exit Sub
        End If

        lblAccountDescr.Text = dt.Rows(0)("BankDescr") & " - " & dt.Rows(0)("AccountDescr") & " Σύνολο : "
        lblAccountTotal.Text = FormatCurrency(dt.Rows(0)("TotalAmount"))

        query = <sql>
                    select *
                    from account_detail_tab d
                    where d.AccountId =  <%= pAccountId %>
                    and d.UserId =  <%= frmMain.pUserId %>
                    and d.AccountDetailDate <%= " <=  #" %><%= Format(dtpDate.Value.Date, "MM/dd/yyyy") & "#" %>
                    order by d.AccountDetailDate desc, d.AccountDetailId desc
                </sql>.Value
        adapter = New OleDbDataAdapter(query, db.con)
        dt = New DataTable
        Try
            adapter.Fill(dt)
        Catch ex As Exception
            MsgBox(ex.Message, vbExclamation, "Αρέθων")
            Exit Sub
        Finally
            adapter.Dispose()
        End Try

        Dim col1 As New DataColumn With {
            .DataType = System.Type.GetType("System.String"),
            .ColumnName = "AccountDetailType"
        }

        dt.Columns.Add(col1)


        For i As Integer = 0 To dt.Rows.Count - 1
            If dt.Rows(i)("SignAmount") < 0 Then
                dt.Rows(i)("AccountDetailType") = "Χρέωση"
            Else
                dt.Rows(i)("AccountDetailType") = "Πίστωση"
            End If
        Next

        Me.uGrd.DataSource = Nothing
        Me.uGrd.DataSource = dt
        Me.uGrd.Show()

        HandleGrid()
    End Sub
    Private Sub HandleGrid()
        uGrd.DisplayLayout.GroupByBox.Hidden = False
        uGrd.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.True
        uGrd.DisplayLayout.Bands(0).Override.HeaderAppearance.TextHAlign = Infragistics.Win.HAlign.Center
        For Each c As UltraGridColumn In uGrd.Rows.Band.Columns
            c.Hidden = True
            c.CellAppearance.TextVAlign = VAlign.Middle
        Next

        uGrd.DisplayLayout.Override.RowSizingAutoMaxLines = 3
        'Babis 2020_07_03 σχολιάστηκε γιατί δεν παίζει σε αυτή την έκδοση μαζί με το PerformAutoSize, γι΄αυτό και ορίστηκε ColHeaderLines = 3 και στα cols που θέλαμε WrapText
        'στο Header.Caption βάλαμε & vbNewLine &, π.χ. .Columns("MMX_PROG").Header.Caption = "Ποσότητα " & vbNewLine & "Ζήτησης / " & vbNewLine & "Δέσμευσης"
        'ugrd.DisplayLayout.Override.WrapHeaderText = Infragistics.Win.DefaultableBoolean.True
        uGrd.Rows.Band.ColHeaderLines = 3

        uGrd.DisplayLayout.Override.AllowColSizing = Infragistics.Win.UltraWinGrid.AllowColSizing.Free
        With uGrd.Rows.Band
            .Columns("AccountDetailDate").Header.Caption = "Ημερομηνία"
            .Columns("AccountDetailType").Header.Caption = "Κίνηση"
            .Columns("Amount").Header.Caption = "Ποσό"
            .Columns("Amount").CellAppearance.TextHAlign = HAlign.Right
            .Columns("Amount").Format = "#,##0.00 €"
            .Columns("Notes").Header.Caption = "Σχόλια"

            .Columns("AccountDetailDate").Hidden = False
            .Columns("AccountDetailType").Hidden = False
            .Columns("Amount").Hidden = False
            .Columns("Notes").Hidden = False

            .Columns("Notes").Width = 1000

            .Columns("AccountDetailDate").Header.VisiblePosition = 1
            .Columns("AccountDetailType").Header.VisiblePosition = 2
            .Columns("Amount").Header.VisiblePosition = 3
            .Columns("Notes").Header.VisiblePosition = 4
        End With

        For i = 0 To uGrd.Rows.Count - 1
            If uGrd.Rows(i).Cells("SignAmount").Value < 0 Then
                uGrd.Rows(i).Cells("Amount").Appearance.ForeColor = Drawing.Color.Red
            Else
                uGrd.Rows(i).Cells("Amount").Appearance.ForeColor = Drawing.Color.Blue
            End If
        Next

        'With uGrd.Rows.Band
        '    .Summaries.Clear()
        '    .SummaryFooterCaption = "ΣΥΝΟΛΑ"

        '    .Summaries.Add("SumAmount", Infragistics.Win.UltraWinGrid.SummaryType.Sum, .Columns("Amount"))
        '    .Summaries(0).ShowCalculatingText = Infragistics.Win.DefaultableBoolean.False
        'End With
    End Sub
    Private Sub uGrd_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles uGrd.MouseDown
        Dim grd_ As UltraGrid, element As Infragistics.Win.UIElement
        Dim row_ As UltraGridRow
        Dim cell_ As UltraGridCell

        grd_ = CType(sender, UltraGrid)

        element = grd_.DisplayLayout.UIElement.ElementFromPoint(New Drawing.Point(e.X, e.Y))
        If element Is Nothing Then Exit Sub

        row_ = element.GetContext(GetType(UltraGridRow))
        cell_ = element.GetContext(GetType(UltraGridCell))

        uGrd.Selected.Rows.Clear()

        If row_ IsNot Nothing AndAlso cell_ IsNot Nothing AndAlso Not IsDBNull(row_.Cells("AccountId").Value) Then
            row_.Activate()
            row_.Selected = True
        End If

        'If e.Button = Windows.Forms.MouseButtons.Right Then
        '    Try
        '        Me.mnuOrder.Show(uGrd, e.Location)
        '    Catch ex As Exception
        '        MsgBox(ex.Message, MsgBoxStyle.Exclamation, "ELVIAL")
        '    End Try
        'ElseIf e.Button = Windows.Forms.MouseButtons.Left Then
        '    If Not row_ Is Nothing AndAlso Not cell_ Is Nothing AndAlso cell_.Column.Key.ToUpper = "ENT_COMMENTS" Then
        '        If ElUtil.CheckDBNull(row_.Cells("DOCNUMBER").Value, ElUtil.enumObjectType.IntType) > 0 Then
        '            row_.Selected = True
        '            CellComments_Click(row_.Cells("ID_ENTOLH_F").Value, row_.Cells("BM1").Value, row_.Cells("ENT_COMMENTS").Value)
        '        End If
        '    End If
        'End If

    End Sub
    Private Sub Button_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        pAccountId = sender.tag
        FilluGrd(pAccountId)
    End Sub

    Private Sub AccountButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Select Case pState
            Case 0
                CreateSynopsis()
            Case 1
                FilluGrd(pAccountId)
        End Select

    End Sub

    Public Class MyButton
        Inherits Button
        Public Sub New()
            MyBase.New()
            SetStyle(ControlStyles.StandardDoubleClick, True)
            SetStyle(ControlStyles.StandardClick, True)
        End Sub
    End Class

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        CreateSynopsis()
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        If pAccountId = 0 Then
            Exit Sub
        End If

        Dim form As New frmAccountDetail
        form.Text = "Νέα κίνηση"
        form.pAccountId = pAccountId
        form.pAction = 0
        form.ShowDialog()

        If form.pReturn Then
            FilluGrd(pAccountId)
        End If

    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        If pAccountId = 0 Then
            Exit Sub
        End If

        Dim form As New frmAccountDetail
        form.Text = "Επεξεργασία κίνησης"
        form.pAccountId = pAccountId
        form.pAccountDetailId = uGrd.ActiveRow.Cells("AccountDetailId").Value
        form.dtpDate.Value = uGrd.ActiveRow.Cells("AccountDetailDate").Value
        form.pAmount = uGrd.ActiveRow.Cells("Amount").Value
        form.pSignAmount = uGrd.ActiveRow.Cells("SignAmount").Value
        form.txtNotes.Text = uGrd.ActiveRow.Cells("Notes").Value
        form.pAction = 1
        form.ShowDialog()

        If form.pReturn Then
            FilluGrd(pAccountId)
        End If
    End Sub

    Private Sub btnCopy_Click(sender As Object, e As EventArgs) Handles btnCopy.Click
        If pAccountId = 0 Then
            Exit Sub
        End If

        Dim form As New frmAccountDetail
        form.Text = "Νέα όμοια κίνηση"
        form.pAccountId = pAccountId
        form.pAccountDetailId = uGrd.ActiveRow.Cells("AccountDetailId").Value
        form.txtAmount.Value = uGrd.ActiveRow.Cells("Amount").Value
        form.pAmount = uGrd.ActiveRow.Cells("Amount").Value
        form.pSignAmount = uGrd.ActiveRow.Cells("SignAmount").Value
        form.txtNotes.Text = uGrd.ActiveRow.Cells("Notes").Value
        form.pAction = 2
        form.ShowDialog()

        If form.pReturn Then
            FilluGrd(pAccountId)
        End If

    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If pAccountId = 0 Then
            Exit Sub
        End If

        Dim pAccountDetailId As Integer
        pAccountDetailId = uGrd.ActiveRow.Cells("AccountDetailId").Value

        Dim x As Integer = MsgBox("Θα διαγραφεί η κίνηση με ποσό " & FormatCurrency(uGrd.ActiveRow.Cells("Amount").Value.ToString) & " ;", MsgBoxStyle.Question + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, "Αρέθων")
        If x <> vbYes Then
            Exit Sub
        End If

        Dim query As String
        query = <sql>
                    delete from account_detail_tab 
                    where AccountDetailId = <%= pAccountDetailId %>
                </sql>.Value
        Dim con As New OleDbConnection
        con = db.con
        con.Open()
        Dim com As New OleDbCommand(query, con)
        Try
            com.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("Αδύνατη η διαγραφή " & ex.Message, MsgBoxStyle.Exclamation, "Αρέθων")
            con.Close()
        Finally
            com.Dispose()
            con.Close()
        End Try

        FilluGrd(pAccountId)
    End Sub
    Private Sub btnExcel_Click(sender As Object, e As EventArgs) Handles btnExcel.Click
        Dim fname As String = ""
        If uGrd.Rows.Count = 0 Then Exit Sub
        saveDlg.Filter = "Excel Files (*.xls)|*.xls"
        saveDlg.ShowDialog()
        fname = saveDlg.FileName
        If fname.Length = 0 Then Exit Sub

        uGrdExcelExporter.Export(uGrd, fname)
        MsgBox("Επιτυχής εξαγωγή", MsgBoxStyle.Exclamation, "Αρέθων")
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click

        frmMain.Panel1.Controls.Clear()
    End Sub

    Private Sub btnMove_Click(sender As Object, e As EventArgs) Handles btnTransfer.Click
        Dim form As New frmAccountsTransfer
        form.pAccountsDt = pSynopsisDt
        form.ShowDialog()

        If form.pReturn Then
            CreateSynopsis()
        End If
    End Sub

    Public Sub ShowButtons(pShow As Boolean)
        'Reorder toolstrip items
        Me.ToolStrip1.Items.Clear()
        Me.ToolStrip1.Items.Add(btnHome)
        Me.ToolStrip1.Items.Add(btnTransfer)
        Me.ToolStrip1.Items.Add(New ToolStripControlHost(dtpDate))
        Me.ToolStrip1.Items.Add(lblAccountDescr)
        Me.ToolStrip1.Items.Add(lblAccountTotal)

        If Not pShow Then
            For Each pItem In pAccountTypebtnList
                Me.ToolStrip1.Items.Add(pItem)
            Next
        End If

        Me.ToolStrip1.Items.Add(lblDescr)
        Me.ToolStrip1.Items.Add(lblTotal)
        Me.ToolStrip1.Items.Add(btnAdd)
        Me.ToolStrip1.Items.Add(btnEdit)
        Me.ToolStrip1.Items.Add(btnCopy)
        Me.ToolStrip1.Items.Add(btnDelete)
        Me.ToolStrip1.Items.Add(btnClose)
        Me.ToolStrip1.Items.Add(btnExcel)

        btnHome.Visible = pShow
        btnAdd.Visible = pShow
        btnEdit.Visible = pShow
        btnCopy.Visible = pShow
        btnDelete.Visible = pShow
        lblAccountDescr.Visible = pShow
        lblAccountTotal.Visible = pShow
        btnExcel.Visible = pShow

        btnTransfer.Visible = Not pShow
        lblDescr.Visible = Not pShow
        lblTotal.Visible = Not pShow
    End Sub
    Private Sub dtpdate_Mousedown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpDate.ValueChanged
        Select Case pState
            Case 0
                CreateSynopsis()
            Case 1
                FilluGrd(pAccountId)
        End Select
    End Sub

End Class