﻿
Imports System.Data.OleDb
Public Class frmAccountsTransfer

    Public pAccountsDt As DataTable
    Public pReturn As Boolean
    Private Sub frmAccountsTransfer_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        For Each pRow As DataRow In pAccountsDt.Rows
            Me.cmbAccountFrom.Items.Add(pRow("BankDescr") & "-" & pRow("AccountDescr") & " : " & FormatCurrency(pRow("TotalAmount")))
            Me.cmbAccountTo.Items.Add(pRow("BankDescr") & "-" & pRow("AccountDescr") & " : " & FormatCurrency(pRow("TotalAmount")))
        Next

        pReturn = False
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        Dim pAmountFrom As Double
        Dim pSignAmountFrom As Double
        Dim pAmountTo As Double
        Dim pSignAmountTo As Double
        Dim pAccountIdFrom As Integer
        Dim pAccountIdTo As Integer
        Dim pAccountAmount As Double

        If Me.cmbAccountFrom.SelectedIndex = -1 Then
            MsgBox("Επιλέξτε λογαριασμό χρέωσης", MsgBoxStyle.Exclamation, "Αρέθων")
            Exit Sub
        End If

        If Me.cmbAccountTo.SelectedIndex = -1 Then
            MsgBox("Επιλέξτε λογαριασμό πίστωσης", MsgBoxStyle.Exclamation, "Αρέθων")
            Exit Sub
        End If

        pAccountIdFrom = pAccountsDt.Rows(Me.cmbAccountFrom.SelectedIndex)("AccountId")
        pAccountIdTo = pAccountsDt.Rows(Me.cmbAccountTo.SelectedIndex)("AccountId")

        pAccountAmount = pAccountsDt.Rows(Me.cmbAccountFrom.SelectedIndex)("TotalAmount")
        If pAccountAmount = 0 Then
            MsgBox("Επιλέξτε λογαριασμό χρέωσης με χρήματα", MsgBoxStyle.Exclamation, "Αρέθων")
            Exit Sub
        End If

        If pAccountIdFrom = pAccountIdTo Then
            MsgBox("Επιλέξτε διαφορετικούς λογαριασμούς", MsgBoxStyle.Exclamation, "Αρέθων")
            Exit Sub
        End If

        pAmountFrom = CDbl(Me.txtAmount.Text)
        pSignAmountFrom = (-1) * pAmountFrom
        pAmountTo = pAmountFrom
        pSignAmountTo = pAmountTo

        If pAmountFrom > pAccountAmount Then
            MsgBox("O λογαριασμός χρέωσης δεν έχει τόσα χρήματα", MsgBoxStyle.Exclamation, "Αρέθων")
            Exit Sub
        End If

        Dim query As String
        query = <sql>
                    insert into account_detail_tab 
                          ( AccountId,              AccountDetailDate,                 Amount,             
                            SignAmount,             Notes,                             UserId
                           )
                    select * from (
                    select TOP 1
                            <%= pAccountIdFrom %> as AccountId,       '<%= Me.dtpDate.Value.Date.ToString %>' as AccountDetailDate,    <%= pAmountFrom %> as Amount,
                            <%= pSignAmountFrom %> as SignAmount,     '<%= Me.txtNotesFrom.Text %>' as Notes,                     <%= frmMain.pUserId %> as UserId
                    from account_detail_tab
                    union all
                    select  TOP 1
                            <%= pAccountIdTo %> as AccountId,        '<%= Me.dtpDate.Value.Date.ToString %>' as AccountDetailDate,     <%= pAmountTo %> as Amount,
                            <%= pSignAmountTo %> as SignAmount,      '<%= Me.txtNotesTo.Text %>' as Notes,                        <%= frmMain.pUserId %> as UserId
                    from account_detail_tab
                            )
                </sql>.Value

        Dim con As New OleDbConnection
        con = db.con
        con.Open()
        Dim com As New OleDbCommand(query, con)
        Try
            com.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("Αδύνατη η αποθήκευση " & ex.Message, MsgBoxStyle.Exclamation, "Αρέθων")
            con.Close()
        Finally
            com.Dispose()
            con.Close()
            Me.Close()
            pReturn = True
        End Try

    End Sub
End Class