﻿Imports System.Data.OleDb
Imports Infragistics.Win
Imports Infragistics.Win.UltraWinGrid

Public Class frmEnergyMasterCard
    WithEvents dtpDate As New DateTimePicker

    Public pStatementId As Integer

    Private Sub frmEnergyMasterCard_Load(sender As Object, e As EventArgs) Handles Me.Load

        'Reorder toolstrip items
        Me.ToolStrip1.Items.Clear()
        Me.ToolStrip1.Items.Add(New ToolStripControlHost(dtpDate))
        Me.ToolStrip1.Items.Add(lblShopDescr)
        Me.ToolStrip1.Items.Add(txtShopDescr)
        Me.ToolStrip1.Items.Add(lblTotalAmountDescr)
        Me.ToolStrip1.Items.Add(lblTotalAmount)
        Me.ToolStrip1.Items.Add(lblAmountToPayDescr)
        Me.ToolStrip1.Items.Add(lblAmountToPay)
        Me.ToolStrip1.Items.Add(lblBonusAmountDescr)
        Me.ToolStrip1.Items.Add(lblBonusAmount)
        Me.ToolStrip1.Items.Add(ToolStripSeparator1)
        Me.ToolStrip1.Items.Add(btnAdd)
        Me.ToolStrip1.Items.Add(btnEdit)
        Me.ToolStrip1.Items.Add(btnCopy)
        Me.ToolStrip1.Items.Add(btnDelete)
        Me.ToolStrip1.Items.Add(btnDelete)
        Me.ToolStrip1.Items.Add(btnPayCard)
        Me.ToolStrip1.Items.Add(btnClose)
        Me.ToolStrip1.Items.Add(btnExcel)

        FilluGrd()
    End Sub

    Private Sub FilluGrd()
        Dim query As String, dt As New DataTable, adapter As OleDbDataAdapter
        query = <sql>
                    select t.*, s.StatementTypeDescr
                    from EnergyMasterCard t, CreditCardStatements s    
                    where t.StatementTypeId = s.StatementTypeId 
                    and t.UserId =  <%= frmMain.pUserId %>
                    and t.StatementDate <%= " <=  #" %><%= Format(dtpDate.Value.Date, "MM/dd/yyyy") & "#" %>
                    order by t.StatementDate desc, t.StatementId desc
                </sql>.Value

        adapter = New OleDbDataAdapter(query, db.con)

        Try
            adapter.Fill(dt)
        Catch ex As Exception
            MsgBox(ex.Message, vbExclamation, "Αρέθων")
            adapter.Dispose()
            Exit Sub
        Finally
            adapter.Dispose()
        End Try

        If dt.Rows.Count = 0 Then Exit Sub

        Dim pAmountToPay As String = dt.Compute("sum(SignAmount)", "").ToString
        If IsDBNull(pAmountToPay) Then pAmountToPay = 0

        Dim pTotalAmount As Double = CDbl(2700 - CDbl(pAmountToPay))
        Dim pBonusAmount As String = dt.Compute("sum(SignAmount)", "StatementTypeId=3").ToString
        If IsDBNull(pBonusAmount) Then pBonusAmount = 0
        pBonusAmount = (-1) * CDbl(pBonusAmount)

        lblTotalAmount.Text = FormatCurrency(pTotalAmount.ToString)
        lblAmountToPay.Text = FormatCurrency(pAmountToPay)
        lblBonusAmount.Text = FormatCurrency(pBonusAmount)

        Me.uGrd.DataSource = Nothing
        Me.uGrd.DataSource = dt

        HandleGrid()
    End Sub

    Private Sub HandleGrid()
        uGrd.DisplayLayout.GroupByBox.Hidden = False
        uGrd.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.True
        uGrd.DisplayLayout.Bands(0).Override.HeaderAppearance.TextHAlign = Infragistics.Win.HAlign.Center
        For Each c As UltraGridColumn In uGrd.Rows.Band.Columns
            c.Hidden = True
            c.CellAppearance.TextVAlign = VAlign.Middle
        Next

        uGrd.DisplayLayout.Override.RowSizingAutoMaxLines = 3
        'Babis 2020_07_03 σχολιάστηκε γιατί δεν παίζει σε αυτή την έκδοση μαζί με το PerformAutoSize, γι΄αυτό και ορίστηκε ColHeaderLines = 3 και στα cols που θέλαμε WrapText
        'στο Header.Caption βάλαμε & vbNewLine &, π.χ. .Columns("MMX_PROG").Header.Caption = "Ποσότητα " & vbNewLine & "Ζήτησης / " & vbNewLine & "Δέσμευσης"
        'ugrd.DisplayLayout.Override.WrapHeaderText = Infragistics.Win.DefaultableBoolean.True
        uGrd.Rows.Band.ColHeaderLines = 3

        uGrd.DisplayLayout.Override.AllowColSizing = Infragistics.Win.UltraWinGrid.AllowColSizing.Free
        With uGrd.Rows.Band
            .Columns("StatementDate").Header.Caption = "Ημερομηνία"
            .Columns("ShopDescr").Header.Caption = "Κατάστημα"
            .Columns("StatementTypeDescr").Header.Caption = "Κίνηση"
            .Columns("Amount").Header.Caption = "Ποσό"
            .Columns("Amount").CellAppearance.TextHAlign = HAlign.Right
            .Columns("Amount").Format = "#,##0.00 €"
            .Columns("Notes").Header.Caption = "Σχόλια"

            .Columns("StatementDate").Hidden = False
            .Columns("ShopDescr").Hidden = False
            .Columns("StatementTypeDescr").Hidden = False
            .Columns("Amount").Hidden = False
            .Columns("Notes").Hidden = False

            .Columns("ShopDescr").Width = 200
            .Columns("Notes").Width = 1000

            .Columns("StatementDate").Header.VisiblePosition = 1
            .Columns("ShopDescr").Header.VisiblePosition = 2
            .Columns("StatementTypeDescr").Header.VisiblePosition = 3
            .Columns("Amount").Header.VisiblePosition = 4
            .Columns("Notes").Header.VisiblePosition = 5
        End With

        For i = 0 To uGrd.Rows.Count - 1
            Select Case uGrd.Rows(i).Cells("StatementTypeId").Value
                Case 1
                    uGrd.Rows(i).Cells("Amount").Appearance.ForeColor = Drawing.Color.Red
                Case 2
                    uGrd.Rows(i).Cells("Amount").Appearance.ForeColor = Drawing.Color.Blue
                Case 3
                    uGrd.Rows(i).Cells("Amount").Appearance.ForeColor = Drawing.Color.Green
                Case Else

            End Select
        Next

    End Sub

    Private Sub uGrd_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles uGrd.MouseDown
        Dim grd_ As UltraGrid, element As Infragistics.Win.UIElement
        Dim row_ As UltraGridRow
        Dim cell_ As UltraGridCell

        grd_ = CType(sender, UltraGrid)

        element = grd_.DisplayLayout.UIElement.ElementFromPoint(New Drawing.Point(e.X, e.Y))
        If element Is Nothing Then Exit Sub

        row_ = element.GetContext(GetType(UltraGridRow))
        cell_ = element.GetContext(GetType(UltraGridCell))

        uGrd.Selected.Rows.Clear()

        If row_ IsNot Nothing AndAlso cell_ IsNot Nothing AndAlso Not IsDBNull(row_.Cells("StatementId").Value) Then
            row_.Activate()
            row_.Selected = True

            pStatementId = row_.Cells("StatementId").Value
        End If

        'If e.Button = Windows.Forms.MouseButtons.Right Then
        '    Try
        '        Me.mnuOrder.Show(uGrd, e.Location)
        '    Catch ex As Exception
        '        MsgBox(ex.Message, MsgBoxStyle.Exclamation, "ELVIAL")
        '    End Try
        'ElseIf e.Button = Windows.Forms.MouseButtons.Left Then
        '    If Not row_ Is Nothing AndAlso Not cell_ Is Nothing AndAlso cell_.Column.Key.ToUpper = "ENT_COMMENTS" Then
        '        If ElUtil.CheckDBNull(row_.Cells("DOCNUMBER").Value, ElUtil.enumObjectType.IntType) > 0 Then
        '            row_.Selected = True
        '            CellComments_Click(row_.Cells("ID_ENTOLH_F").Value, row_.Cells("BM1").Value, row_.Cells("ENT_COMMENTS").Value)
        '        End If
        '    End If
        'End If

    End Sub

    Private Sub dtpdate_Mousedown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpDate.ValueChanged
        FilluGrd()
    End Sub
    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        If uGrd.ActiveRow Is Nothing Then
            Exit Sub
        End If

        Dim form As New frmEnergyMasterCardDetail
        form.Text = "Νέα κίνηση"
        form.pStatementId = pStatementId
        form.pAction = 0
        form.ShowDialog()

        If form.pReturn Then
            FilluGrd()
        End If

    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        If uGrd.ActiveRow Is Nothing Then
            Exit Sub
        End If

        Dim form As New frmEnergyMasterCardDetail
        form.Text = "Επεξεργασία κίνησης"
        form.pStatementId = pStatementId
        form.dtpDate.Value = uGrd.ActiveRow.Cells("StatementDate").Value
        form.pShopDescr = uGrd.ActiveRow.Cells("ShopDescr").Value
        form.pAmount = uGrd.ActiveRow.Cells("Amount").Value
        form.pStatementTypeId = uGrd.ActiveRow.Cells("StatementTypeId").Value
        form.txtNotes.Text = uGrd.ActiveRow.Cells("Notes").Value
        form.pAction = 1
        form.ShowDialog()

        If form.pReturn Then
            FilluGrd()
        End If
    End Sub

    Private Sub btnCopy_Click(sender As Object, e As EventArgs) Handles btnCopy.Click
        If uGrd.ActiveRow Is Nothing Then
            Exit Sub
        End If

        Dim form As New frmEnergyMasterCardDetail
        form.Text = "Νέα όμοια κίνηση"
        form.pStatementId = pStatementId
        form.pShopDescr = uGrd.ActiveRow.Cells("ShopDescr").Value
        form.txtAmount.Value = uGrd.ActiveRow.Cells("Amount").Value
        form.pAmount = uGrd.ActiveRow.Cells("Amount").Value
        form.pStatementTypeId = uGrd.ActiveRow.Cells("StatementTypeId").Value
        form.txtNotes.Text = uGrd.ActiveRow.Cells("Notes").Value
        form.pAction = 2
        form.ShowDialog()

        If form.pReturn Then
            FilluGrd()
        End If

    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If uGrd.ActiveRow Is Nothing Then
            Exit Sub
        End If

        Dim x As Integer = MsgBox("Θα διαγραφεί η κίνηση με ποσό " & FormatCurrency(uGrd.ActiveRow.Cells("Amount").Value.ToString) & " ;", MsgBoxStyle.Question + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, "Αρέθων")
        If x <> vbYes Then
            Exit Sub
        End If

        pStatementId = uGrd.ActiveRow.Cells("StatementId").Value

        Dim query As String
        query = <sql>
                    delete from EnergyMasterCard 
                    where StatementId = <%= pStatementId %>
                </sql>.Value
        Dim con As New OleDbConnection
        con = db.con
        con.Open()
        Dim com As New OleDbCommand(query, con)
        Try
            com.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("Αδύνατη η διαγραφή " & ex.Message, MsgBoxStyle.Exclamation, "Αρέθων")
            con.Close()
        Finally
            com.Dispose()
            con.Close()
        End Try

        FilluGrd()
    End Sub
    Private Sub btnExcel_Click(sender As Object, e As EventArgs) Handles btnExcel.Click
        Dim fname As String = ""
        If uGrd.Rows.Count = 0 Then Exit Sub
        saveDlg.Filter = "Excel Files (*.xls)|*.xls"
        saveDlg.ShowDialog()
        fname = saveDlg.FileName
        If fname.Length = 0 Then Exit Sub

        uGrdExcelExporter.Export(uGrd, fname)
        MsgBox("Επιτυχής εξαγωγή", MsgBoxStyle.Exclamation, "Αρέθων")
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        frmMain.Panel1.Controls.Clear()
    End Sub

    Private Sub btnPayCard_Click(sender As Object, e As EventArgs) Handles btnPayCard.Click
        Dim form As New frmEnergyMasterCardPay
        form.ShowDialog()

        If form.pReturn Then
            FilluGrd()
        End If
    End Sub

    Private Sub ToolStrip1_ItemClicked(sender As Object, e As ToolStripItemClickedEventArgs) Handles ToolStrip1.ItemClicked

    End Sub
End Class