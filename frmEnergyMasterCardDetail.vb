﻿Imports System.Data.OleDb
Public Class frmEnergyMasterCardDetail

    Public pAction As Integer
    '0 = Add
    '1 = edit
    '2 = copy
    Public pAmount As Double
    Public pStatementId As Integer
    Public pShopDescr As String
    Public pStatementTypeId As Integer
    Public pReturn As Boolean

    Private Sub frmEnergyMasterCardDetail_Load(sender As Object, e As EventArgs) Handles Me.Load
        FillcmbStatementType()
        pReturn = False

        Me.dtpDate.Enabled = True
        Me.txtShopDescr.Enabled = True
        cmbStatementType.Enabled = True

        Select Case pAction
            Case 0
                Me.dtpDate.Value = Now.Date
                Me.txtAmount.Text = 0
                Me.txtNotes.Text = ""
            Case 1
                Me.txtShopDescr.Text = pShopDescr
                Me.txtAmount.Text = pAmount
            Case 2
                Me.dtpDate.Value = Now.Date
                Me.txtShopDescr.Text = pShopDescr
                Me.txtAmount.Text = pAmount
                Me.txtShopDescr.Enabled = False
                cmbStatementType.Enabled = False
        End Select
    End Sub

    Private Sub FillcmbStatementType()
        cmbStatementType.Items.Clear()

        Dim query As String, StatementTypeDt As New DataTable, adapter As OleDbDataAdapter
        query = <sql>
                    select *
                    from CreditCardStatements
                    order by StatementTypeId    
                </sql>.Value

        adapter = New OleDbDataAdapter(query, db.con)

        Try
            adapter.Fill(StatementTypeDt)
        Catch ex As Exception
            MsgBox(ex.Message, vbExclamation, "Αρέθων")
            adapter.Dispose()
            Exit Sub
        Finally
            adapter.Dispose()
        End Try

        For i As Integer = 0 To StatementTypeDt.Rows.Count - 1
            cmbStatementType.Items.Add(StatementTypeDt.Rows(i)("StatementTypeDescr"))
        Next

        If pStatementTypeId = 0 Then
            cmbStatementType.SelectedIndex = 0
        Else
            cmbStatementType.SelectedIndex = pStatementTypeId - 1
        End If

    End Sub
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        Dim query As String
        Dim pSignAmount As Double

        pAmount = CDbl(Me.txtAmount.Text)

        Select Case Me.cmbStatementType.SelectedIndex
            Case 0
                'Χρέωση
                pSignAmount = pAmount
            Case 1, 2
                'Πίστωση, επιστροφή
                pSignAmount = pAmount * (-1)
        End Select

        Select Case pAction
            Case 0, 2
                query = <sql>
                    insert into EnergyMasterCard 
                          ( StatementDate,                         ShopDescr,                           Amount,          StatementTypeId,        
                            SignAmount,                            Notes,                               UserId
                           ) 
                    values( '<%= Me.dtpDate.Value.ToString %>',    '<%= Me.txtShopDescr.Text %>',       <%= pAmount %>,   <%= Me.cmbStatementType.SelectedIndex + 1 %>,
                            <%= pSignAmount %>,                    '<%= Me.txtNotes.Text %>',           <%= frmMain.pUserId %>
                           )                   
                </sql>.Value
            Case 1
                query = <sql>
                    update EnergyMasterCard 
                    set StatementDate = '<%= Me.dtpDate.Value.ToString %>', 
                        ShopDescr =  '<%= Me.txtShopDescr.Text %>',
                        Amount = <%= pAmount %>,
                        StatementTypeId = <%= Me.cmbStatementType.SelectedIndex + 1 %>,
                        SignAmount = <%= pSignAmount %>,             
                        Notes = '<%= Me.txtNotes.Text %>'                       
                    where StatementId = <%= pStatementId %>
                        </sql>.Value
        End Select

        Dim con As New OleDbConnection
        con = db.con
        con.Open()
        Dim com As New OleDbCommand(query, con)
        Try
            com.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("Αδύνατη η αποθήκευση " & ex.Message, MsgBoxStyle.Exclamation, "Αρέθων")
            con.Close()
        Finally
            com.Dispose()
            con.Close()
            Me.Close()
            pReturn = True
        End Try

    End Sub

End Class