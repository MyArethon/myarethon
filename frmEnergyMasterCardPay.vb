﻿Imports System.Data.OleDb
Public Class frmEnergyMasterCardPay

    Public pReturn As Boolean

    Private pAccountsDt As DataTable
    Private Sub frmEnergyMasterCardPay_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        pReturn = False

        Dim query As String, adapter As OleDbDataAdapter
        pAccountsDt = New DataTable
        query = <sql>
                    select h.BankId, b.BankDescr, h.AccountDescr, d.AccountId, sum(d.SignAmount) as TotalAmount
                    from account_detail_tab d, account_header_tab h, bank_tab b
                    where h.AccountId = d.AccountId
                    and h.BankId = b.BankId
                    and h.inUse = -1
                    and d.UserId =  <%= frmMain.pUserId %>
                    and d.AccountDetailDate <%= " <=  #" %><%= Format(dtpDate.Value.Date, "MM/dd/yyyy") & "#" %>
                    and h.AccountTypeId = 1
                    group by h.BankId, b.BankDescr, h.AccountDescr, d.AccountId
                </sql>.Value

        adapter = New OleDbDataAdapter(query, db.con)

        Try
            adapter.Fill(pAccountsDt)
        Catch ex As Exception
            MsgBox(ex.Message, vbExclamation, "Αρέθων")
            adapter.Dispose()
            Exit Sub
        Finally
            adapter.Dispose()
        End Try

        If pAccountsDt.Rows.Count = 0 Then Exit Sub

        For Each pRow As DataRow In pAccountsDt.Rows
            Me.cmbAccountFrom.Items.Add(pRow("BankDescr") & "-" & pRow("AccountDescr") & " : " & FormatCurrency(pRow("TotalAmount")))
        Next

    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        Dim pAmountFrom As Double
        Dim pSignAmountFrom As Double
        Dim pAmountTo As Double
        Dim pSignAmountTo As Double
        Dim pAccountIdFrom As Integer
        Dim pBankDescr As String
        Dim pAccountAmount As Double

        If Me.cmbAccountFrom.SelectedIndex = -1 Then
            MsgBox("Επιλέξτε λογαριασμό χρέωσης", MsgBoxStyle.Exclamation, "Αρέθων")
            Exit Sub
        End If

        pAccountIdFrom = pAccountsDt.Rows(Me.cmbAccountFrom.SelectedIndex)("AccountId")
        pBankDescr = pAccountsDt.Rows(Me.cmbAccountFrom.SelectedIndex)("BankDescr")

        pAccountAmount = pAccountsDt.Rows(Me.cmbAccountFrom.SelectedIndex)("TotalAmount")
        If pAccountAmount = 0 Then
            MsgBox("Επιλέξτε λογαριασμό χρέωσης με χρήματα", MsgBoxStyle.Exclamation, "Αρέθων")
            Exit Sub
        End If

        pAmountFrom = CDbl(Me.txtAmount.Text)
        pSignAmountFrom = (-1) * pAmountFrom
        pAmountTo = pAmountFrom
        pSignAmountTo = (-1) * pAmountTo

        If pAmountFrom > pAccountAmount Then
            MsgBox("O λογαριασμός χρέωσης δεν έχει τόσα χρήματα", MsgBoxStyle.Exclamation, "Αρέθων")
            Exit Sub
        End If

        Dim con As New OleDbConnection
        con = db.con
        Dim ptrans As OleDbTransaction = Nothing
        Try
            con.Open()
            ptrans = con.BeginTransaction

            Dim query1 As String, query2 As String
            query1 = <sql>
                     insert into account_detail_tab 
                           ( AccountId,              AccountDetailDate,                                Amount,             
                             SignAmount,             Notes,                                            UserId
                            )
                     values (
                             <%= pAccountIdFrom %>,  '<%= Me.dtpDate.Value.Date.ToString %>',               <%= pAmountFrom %>,
                             <%= pSignAmountFrom %>, '<%= "Πληρωμή πιστωτικής Energy Mastercard" %>', <%= frmMain.pUserId %>
                             )
                 </sql>.Value

            query2 = <sql>
                     insert into EnergyMasterCard 
                           ( StatementDate,                         ShopDescr,                                       Amount,             StatementTypeId,        
                             SignAmount,                            Notes,                                           UserId
                            ) 
                     values( '<%= Me.dtpDate.Value.Date.ToString %>',    '<%= pBankDescr %>',                             <%= pAmountTo %>,   <%= 2 %>,
                              <%= pSignAmountTo %>,                 '<%= "Πληρωμή πιστωτικής Energy Mastercard" %>', <%= frmMain.pUserId %>
                            ) 
                 </sql>.Value

            Dim com1 As New OleDbCommand(query1, con)
            com1.Transaction = ptrans
            Try
                com1.ExecuteNonQuery()
            Catch ex As Exception
                MsgBox("Αδύνατη η αποθήκευση " & ex.Message, MsgBoxStyle.Exclamation, "Αρέθων")
                ptrans.Rollback()
                con.Close()
                Exit Sub
            Finally
            End Try

            Dim com2 As New OleDbCommand(query2, con)
            com2.Transaction = ptrans
            Try
                com2.ExecuteNonQuery()
            Catch ex As Exception
                MsgBox("Αδύνατη η αποθήκευση " & ex.Message, MsgBoxStyle.Exclamation, "Αρέθων")
                ptrans.Rollback()
                con.Close()
                Exit Sub
            Finally

            End Try

            ptrans.Commit()

        Catch ex As Exception
            ptrans.Rollback()
        Finally
            con.Close()
            Me.Close()
            pReturn = True
        End Try

    End Sub
End Class