Imports System.Data.OleDb

Public Class frmLogin

    Public pReturn As Boolean
    Private Sub frmLogin_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        pReturn = False

        If My.Settings.pTestDb = 1 Then
            Me.txtUsername.Text = "Babis"
            Me.txtPassword.Text = " "
        End If

    End Sub
    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        CheckUser(Me.txtUsername.Text, Me.txtPassword.Text)
    End Sub
    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub
    Private Sub CheckUser(pUsername As String, pPassword As String)

        Dim query As String, dt As New DataTable, adapter As OleDbDataAdapter
        query = <sql>
                    select *
                    from users
                    where username  = '<%= pUsername %>'
                </sql>.Value
        adapter = New OleDbDataAdapter(query, db.con)

        Try
            adapter.Fill(dt)
        Catch ex As Exception
            MsgBox(ex.Message, vbExclamation, "������")
        Finally
            adapter.Dispose()
        End Try

        If dt.Rows.Count = 0 Then
            MsgBox("��� ������� � �������", vbExclamation, "������")
        ElseIf dt.Rows.Count > 1 Then
            MsgBox("�������� �� �� ��������, ������������� �� ��� �����������", vbExclamation, "������")
        ElseIf dt.Rows.Count = 1 Then
            If dt.Rows(0)("UserPassword") <> pPassword Then
                MsgBox("����� ������� ������", vbExclamation, "������")
            Else
                frmMain.pUserId = dt.Rows(0)("UserId")
                pReturn = True
                Me.Close()
            End If
        End If
    End Sub

End Class
