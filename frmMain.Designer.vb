﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim RibbonTab1 As Infragistics.Win.UltraWinToolbars.RibbonTab = New Infragistics.Win.UltraWinToolbars.RibbonTab("tMain")
        Dim RibbonGroup1 As Infragistics.Win.UltraWinToolbars.RibbonGroup = New Infragistics.Win.UltraWinToolbars.RibbonGroup("gAccount")
        Dim RibbonGroup2 As Infragistics.Win.UltraWinToolbars.RibbonGroup = New Infragistics.Win.UltraWinToolbars.RibbonGroup("gCar")
        Dim RibbonGroup3 As Infragistics.Win.UltraWinToolbars.RibbonGroup = New Infragistics.Win.UltraWinToolbars.RibbonGroup("gHome")
        Me.UltraSplitter1 = New Infragistics.Win.Misc.UltraSplitter()
        Me.frmMain_Fill_Panel = New Infragistics.Win.Misc.UltraPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me._frmMain_Toolbars_Dock_Area_Right = New Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea()
        Me.ribbon_ = New Infragistics.Win.UltraWinToolbars.UltraToolbarsManager(Me.components)
        Me._frmMain_Toolbars_Dock_Area_Left = New Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea()
        Me._frmMain_Toolbars_Dock_Area_Bottom = New Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea()
        Me._frmMain_Toolbars_Dock_Area_Top = New Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea()
        Me.frmMain_Fill_Panel.ClientArea.SuspendLayout()
        Me.frmMain_Fill_Panel.SuspendLayout()
        CType(Me.ribbon_, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UltraSplitter1
        '
        Me.UltraSplitter1.Location = New System.Drawing.Point(0, 0)
        Me.UltraSplitter1.Name = "UltraSplitter1"
        Me.UltraSplitter1.RestoreExtent = 0
        Me.UltraSplitter1.Size = New System.Drawing.Size(6, 1070)
        Me.UltraSplitter1.TabIndex = 1
        '
        'frmMain_Fill_Panel
        '
        '
        'frmMain_Fill_Panel.ClientArea
        '
        Me.frmMain_Fill_Panel.ClientArea.Controls.Add(Me.Panel1)
        Me.frmMain_Fill_Panel.ClientArea.Controls.Add(Me.UltraSplitter1)
        Me.frmMain_Fill_Panel.Cursor = System.Windows.Forms.Cursors.Default
        Me.frmMain_Fill_Panel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.frmMain_Fill_Panel.Location = New System.Drawing.Point(5, 204)
        Me.frmMain_Fill_Panel.Name = "frmMain_Fill_Panel"
        Me.frmMain_Fill_Panel.Size = New System.Drawing.Size(2509, 1070)
        Me.frmMain_Fill_Panel.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(6, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(2503, 1070)
        Me.Panel1.TabIndex = 2
        '
        '_frmMain_Toolbars_Dock_Area_Right
        '
        Me._frmMain_Toolbars_Dock_Area_Right.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me._frmMain_Toolbars_Dock_Area_Right.AlphaBlendMode = Infragistics.Win.AlphaBlendMode.Disabled
        Me._frmMain_Toolbars_Dock_Area_Right.BackColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._frmMain_Toolbars_Dock_Area_Right.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Right
        Me._frmMain_Toolbars_Dock_Area_Right.ForeColor = System.Drawing.SystemColors.ControlText
        Me._frmMain_Toolbars_Dock_Area_Right.InitialResizeAreaExtent = 5
        Me._frmMain_Toolbars_Dock_Area_Right.Location = New System.Drawing.Point(2514, 204)
        Me._frmMain_Toolbars_Dock_Area_Right.Name = "_frmMain_Toolbars_Dock_Area_Right"
        Me._frmMain_Toolbars_Dock_Area_Right.Size = New System.Drawing.Size(5, 1070)
        Me._frmMain_Toolbars_Dock_Area_Right.ToolbarsManager = Me.ribbon_
        '
        'ribbon_
        '
        Me.ribbon_.AlphaBlendMode = Infragistics.Win.AlphaBlendMode.Disabled
        Me.ribbon_.AlwaysShowMenusExpanded = Infragistics.Win.DefaultableBoolean.[True]
        Me.ribbon_.DesignerFlags = 1
        Me.ribbon_.DockWithinContainer = Me
        Me.ribbon_.DockWithinContainerBaseType = GetType(System.Windows.Forms.Form)
        Me.ribbon_.FormDisplayStyle = Infragistics.Win.UltraWinToolbars.FormDisplayStyle.StandardWithRibbon
        Me.ribbon_.IsGlassSupported = False
        Me.ribbon_.MiniToolbar.ToolRowCount = 4
        Me.ribbon_.Office2007UICompatibility = False
        RibbonTab1.Caption = "Διαχείριση"
        RibbonGroup1.Caption = "Οικονομικά"
        RibbonGroup1.PreferredToolSize = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large
        RibbonGroup2.Caption = "Αυτοκίνητο"
        RibbonGroup2.PreferredToolSize = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large
        RibbonGroup3.Caption = "Σπίτι"
        RibbonGroup3.PreferredToolSize = Infragistics.Win.UltraWinToolbars.RibbonToolSize.Large
        RibbonTab1.Groups.AddRange(New Infragistics.Win.UltraWinToolbars.RibbonGroup() {RibbonGroup1, RibbonGroup2, RibbonGroup3})
        Me.ribbon_.Ribbon.NonInheritedRibbonTabs.AddRange(New Infragistics.Win.UltraWinToolbars.RibbonTab() {RibbonTab1})
        Me.ribbon_.Ribbon.Visible = True
        Me.ribbon_.ShowFullMenusDelay = 500
        Me.ribbon_.ToolbarSettings.ToolDisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText
        Me.ribbon_.ToolTipDisplayStyle = Infragistics.Win.UltraWinToolbars.ToolTipDisplayStyle.Standard
        '
        '_frmMain_Toolbars_Dock_Area_Left
        '
        Me._frmMain_Toolbars_Dock_Area_Left.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me._frmMain_Toolbars_Dock_Area_Left.AlphaBlendMode = Infragistics.Win.AlphaBlendMode.Disabled
        Me._frmMain_Toolbars_Dock_Area_Left.BackColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._frmMain_Toolbars_Dock_Area_Left.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Left
        Me._frmMain_Toolbars_Dock_Area_Left.ForeColor = System.Drawing.SystemColors.ControlText
        Me._frmMain_Toolbars_Dock_Area_Left.InitialResizeAreaExtent = 5
        Me._frmMain_Toolbars_Dock_Area_Left.Location = New System.Drawing.Point(0, 204)
        Me._frmMain_Toolbars_Dock_Area_Left.Name = "_frmMain_Toolbars_Dock_Area_Left"
        Me._frmMain_Toolbars_Dock_Area_Left.Size = New System.Drawing.Size(5, 1070)
        Me._frmMain_Toolbars_Dock_Area_Left.ToolbarsManager = Me.ribbon_
        '
        '_frmMain_Toolbars_Dock_Area_Bottom
        '
        Me._frmMain_Toolbars_Dock_Area_Bottom.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me._frmMain_Toolbars_Dock_Area_Bottom.AlphaBlendMode = Infragistics.Win.AlphaBlendMode.Disabled
        Me._frmMain_Toolbars_Dock_Area_Bottom.BackColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._frmMain_Toolbars_Dock_Area_Bottom.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Bottom
        Me._frmMain_Toolbars_Dock_Area_Bottom.ForeColor = System.Drawing.SystemColors.ControlText
        Me._frmMain_Toolbars_Dock_Area_Bottom.InitialResizeAreaExtent = 5
        Me._frmMain_Toolbars_Dock_Area_Bottom.Location = New System.Drawing.Point(0, 1274)
        Me._frmMain_Toolbars_Dock_Area_Bottom.Name = "_frmMain_Toolbars_Dock_Area_Bottom"
        Me._frmMain_Toolbars_Dock_Area_Bottom.Size = New System.Drawing.Size(2519, 5)
        Me._frmMain_Toolbars_Dock_Area_Bottom.ToolbarsManager = Me.ribbon_
        '
        '_frmMain_Toolbars_Dock_Area_Top
        '
        Me._frmMain_Toolbars_Dock_Area_Top.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me._frmMain_Toolbars_Dock_Area_Top.AlphaBlendMode = Infragistics.Win.AlphaBlendMode.Disabled
        Me._frmMain_Toolbars_Dock_Area_Top.BackColor = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._frmMain_Toolbars_Dock_Area_Top.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Top
        Me._frmMain_Toolbars_Dock_Area_Top.ForeColor = System.Drawing.SystemColors.ControlText
        Me._frmMain_Toolbars_Dock_Area_Top.Location = New System.Drawing.Point(0, 0)
        Me._frmMain_Toolbars_Dock_Area_Top.Name = "_frmMain_Toolbars_Dock_Area_Top"
        Me._frmMain_Toolbars_Dock_Area_Top.Size = New System.Drawing.Size(2519, 204)
        Me._frmMain_Toolbars_Dock_Area_Top.ToolbarsManager = Me.ribbon_
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(2519, 1279)
        Me.Controls.Add(Me.frmMain_Fill_Panel)
        Me.Controls.Add(Me._frmMain_Toolbars_Dock_Area_Left)
        Me.Controls.Add(Me._frmMain_Toolbars_Dock_Area_Right)
        Me.Controls.Add(Me._frmMain_Toolbars_Dock_Area_Bottom)
        Me.Controls.Add(Me._frmMain_Toolbars_Dock_Area_Top)
        Me.Name = "frmMain"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Αρέθων"
        Me.frmMain_Fill_Panel.ClientArea.ResumeLayout(False)
        Me.frmMain_Fill_Panel.ResumeLayout(False)
        CType(Me.ribbon_, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents UltraSplitter1 As Infragistics.Win.Misc.UltraSplitter
    Friend WithEvents frmMain_Fill_Panel As Infragistics.Win.Misc.UltraPanel
    Friend WithEvents _frmMain_Toolbars_Dock_Area_Right As Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea
    Friend WithEvents _frmMain_Toolbars_Dock_Area_Left As Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea
    Friend WithEvents _frmMain_Toolbars_Dock_Area_Bottom As Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea
    Friend WithEvents _frmMain_Toolbars_Dock_Area_Top As Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea
    Friend WithEvents Panel1 As Panel
    Public WithEvents ribbon_ As Infragistics.Win.UltraWinToolbars.UltraToolbarsManager
End Class
