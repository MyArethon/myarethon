﻿Imports Infragistics.Win.UltraWinToolbars

Public Class frmMain

    Public Shared pUserId As Integer
    Public pLoginOk As Boolean
    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Hide()
        pLoginOk = False

        Select Case My.Settings.pTestDb
            Case 0
                Me.Text = "Αρέθων - Live"
            Case 1
                Me.Text = "Αρέθων - Δοκιμαστική βάση"
        End Select


        Dim form As New frmLogin
        form.ShowDialog()
        pLoginOk = form.pReturn

        If pLoginOk Then
            Me.Show()
        Else
            Me.Close()
        End If

        FillRibbon()

    End Sub

#Region "Ribbon"
    Private Function CreateRibbonButtonTool(ByRef group As RibbonGroup, ByVal tool_key As String, ByVal caption As String, ByVal image As System.Drawing.Bitmap, ByVal enable As Boolean, ByVal visible As Boolean) As ToolBase
        Dim cmdTool As ButtonTool


        ribbon_.Tools.Add(New ButtonTool(tool_key))

        cmdTool = ribbon_.Tools(tool_key)

        cmdTool.SharedProps.Caption = caption
        cmdTool.CustomizedImage = image
        cmdTool.SharedProps.Enabled = enable
        cmdTool.SharedProps.Visible = visible
        group.Tools.AddTool(tool_key)
        Return cmdTool
    End Function
    Private Function CreateRibbonTextBoxTool(ByRef group As RibbonGroup, ByVal tool_key As String, ByVal caption As String, ByVal locked As Boolean, ByVal text As String, ByVal visible As Boolean) As ToolBase
        Dim cmdTool As New TextBoxTool(tool_key)


        ribbon_.Tools.Add(cmdTool)

        cmdTool = ribbon_.Tools(tool_key)

        cmdTool.SharedProps.Caption = caption
        cmdTool.Locked = locked
        cmdTool.Text = text
        cmdTool.SharedProps.Visible = visible

        group.Tools.AddTool(tool_key)
        Return cmdTool
    End Function
    Private Sub FillRibbon()
        'Οικονομικά 
        CreateRibbonButtonTool(ribbon_.Ribbon.Tabs("tMain").Groups("gAccount"), "cmdAccountSettings", "Ρυθμίσεις", My.Resources.Double_J_Design_Ravenna_3d_Settings.ToBitmap, True, True)
        CreateRibbonButtonTool(ribbon_.Ribbon.Tabs("tMain").Groups("gAccount"), "cmdBankAccounts", "Λογαριασμοί", My.Resources.Cjdowner_Cryptocurrency_Euro_EUR, True, True)
        CreateRibbonButtonTool(ribbon_.Ribbon.Tabs("tMain").Groups("gAccount"), "cmdDebts", "Οφειλές", My.Resources.Aha_Soft_Business_Toolbar_Payment.ToBitmap, True, True)
        CreateRibbonButtonTool(ribbon_.Ribbon.Tabs("tMain").Groups("gAccount"), "cmdEnergyMastercard", "Πιστωτική Energy", My.Resources.Double_J_Design_Ravenna_3d_Credit_Card.ToBitmap, True, True)

        'Αυτοκίνητο
        CreateRibbonButtonTool(ribbon_.Ribbon.Tabs("tMain").Groups("gCar"), "cmdCarSettings", "Ρυθμίσεις", My.Resources.Double_J_Design_Ravenna_3d_Settings.ToBitmap, True, True)
        CreateRibbonButtonTool(ribbon_.Ribbon.Tabs("tMain").Groups("gCar"), "cmdKiaCeed", "Kia Ceed", My.Resources.Calebamesbury_Classic_American_Cars_Muscle_Car_Chevrolet_Camaro_SS.ToBitmap, True, True)

        'Σπίτι
        CreateRibbonButtonTool(ribbon_.Ribbon.Tabs("tMain").Groups("gHome"), "cmdHomeSettings", "Ρυθμίσεις", My.Resources.Double_J_Design_Ravenna_3d_Settings.ToBitmap, True, True)
        CreateRibbonButtonTool(ribbon_.Ribbon.Tabs("tMain").Groups("gHome"), "cmdHomeOutgoings", "'Εξοδα Σπιτιού", My.Resources.Fasticon_Comic_Tiger_Home.ToBitmap, True, True)
    End Sub
    Private Sub ribbon__ToolClick(sender As Object, e As ToolClickEventArgs) Handles ribbon_.ToolClick
        Select Case e.Tool.Key
            Case "cmdAccountSettings"
                Dim form As New frmTest
                form.Show()
            Case "cmdBankAccounts"
                Dim form As New frmAccounts
                Me.Panel1.Controls.Add(form)
                form.Dock = DockStyle.Fill
            Case "cmdDebts"
                Dim form As New frmAccounts
                form.pDebts = True
                Me.Panel1.Controls.Add(form)
                form.Dock = DockStyle.Fill
            Case "cmdEnergyMastercard"
                Dim form As New frmEnergyMasterCard
                Me.Panel1.Controls.Add(form)
                form.Dock = DockStyle.Fill
            Case "cmdCarSettings"

            Case "cmdKiaCeed"

        End Select

    End Sub
    Private Sub frmMain_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If pLoginOk Then

            Dim x As Integer = MsgBox("Θα πραγματοποιηθεί έξοδος, να συνεχίσω ;", MsgBoxStyle.Question + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, "Αρέθων")
            If x <> vbYes Then
                e.Cancel = True
            End If
        End If
    End Sub
#End Region

End Class
